.. -*- restructuredtext -*-
=================================
 goenv -- Go environment manager
=================================

1. What's this
==============

``goenv`` is command line tool to create Go project.


2. Install
==========

2.0 pre-requirements
--------------------

* In advance of using installation scripts, please make sure directories specified by ``$GOENVGOROOT`` and ``$GOENVTARGET`` exist and those permissions are correctly set.

* Make sure ``git`` and ``mercurial`` are installed.

2.1 Fast installation
---------------------

If you are already using Go and have exising GOROOT with built Go binaries and you do not need to install Go binaries with goof command, install goenv with fast-install.sh.

To install ``goenv``, get all code for it and put built program into appropriate directory::

   $ git clone https://bitbucket.org/ymotongpoo/goenv
   $ cd goenv
   $ go build -o goenv *.go
   $ chmod +x goenv
   $ cp goenv /path/to/bindir

or you can run a following command for procedure all above::

   $ export GOENVTARGET=/path/to/install/target
   $ curl -L https://bitbucket.org/ymotongpoo/goenv/raw/master/shellscripts/fast-install.sh | zsh

(Author is using zsh)

2.2 Go initial installation
---------------------------

You have to set following variables beforehand:

* ``GOENVGOROOT``: Go binary install target. All Go binaries are installed under this directory. 
* ``GOENVTARGET``: goenv install target. goenv and goenvwrapper.sh are installed here.
* ``GOENVHOME``: All workspaces created with ``goof`` command are here.

ex) ``~/.zshrc``::

  export GOENVGOROOT=$HOME/.goenvs
  export GOENVTARGET=$HOME/bin
  export GOENVHOME=$HOME/workspace

If you're fine with installing Go from the source code and goenv at the same time, use initial-install.sh::

   $ curl -L https://bitbucket.org/ymotongpoo/goenv/raw/master/shellscripts/initial-install.sh | zsh

If you feel anxious to the warning message as shown below::

   warning: code.google.com certificate with fingerprint c3:32:c3:b6:4b:d0:87:78:ff:ab:20:6f:9b:9e:3b:c2:a9:b1:0c:eb not verified (check hostfingerprints or web.cacerts config setting)

Then you can adding ``hostfingerprints`` section to ``~/.hgrc`` configuration file for Mercurial::

  [hostfingerprints]
  code.google.com = c3:32:c3:b6:4b:d0:87:78:ff:ab:20:6f:9b:9e:3b:c2:a9:b1:0c:eb

If you got error messages as below and failed checkout the repository of go, you have to check ``hostfingerprints``::

  abort: certificate for code.google.com has unexpected fingerprint c3:32:c3:b6:4b:d0:87:78:ff:ab:20:6f:9b:9e:3b:c2:a9:b1:0c:eb

When the installation that ``release`` version of golang is finished, you can set the ``GOROOT`` variable and adding ``$GOROOT/bin`` to ``$PATH``.
ex) ``~/.zshrc``::

  export GOROOT=$GOENVGOROOT/release
  export PATH=$GOROOT/bin:$PATH

3. Usage
========

3.1 Basics
----------

``goenv`` is quite simple tool and usage is as following::

   goenv [-go GOROOT] [-deps <additional GOPATH>] [-gae] envname
   goenv -version

``-go`` option is used when we need to use different Go binary.

``-deps`` option is for adding ``GOPATH`` in case that the projects depends on local 3rd party packages.

For instance, when you are to create independent project ``foo``, just run::

   goenv foo

It create directory ``foo`` in following manner::

   % tree foo
   foo/
    ├── activate
    └── src/ 

To activate ``foo`` project's environment, load ``activate`` file and required environment variables will be set properly::

   % source foo/activate
   (go:foo) %

In order to deactivate current environment and rollback to what it was, call ``deactivate`` function::

   (go:foo) % deactivate
   % 

3.2 GAE/Go support
------------------

``goenv`` also supports creating GAE/Go enrionments. With ``-gae`` option, it generates ``app.yaml`` and initial Go file for GAE/Go as well as ``activate`` script.
For instance, if you create a workspace ``gaetest`` with this option, directory sructure of a generate workspace would be::

   % goenv -gae gaetest
   % tree gaetest 
   gaetest
   ├── activate
   └── src
          ├── app.yaml
          └── gaetest.go

At the same time, ``-gae`` option sets path to ``"$GAEGO"`` at the head of ``PATH``, where ``GAEGO`` is path to Google App Engine for Go SDK.


4. Support tool (``goof``)
==========================

4.1 Basics
----------

``goof`` -- goenv wrapper for goofy gophers -- command is provided from goenvwrapper.sh to enhance the power of ``goenv``.
install.sh will put goenvwrapper.sh in the same directory as ``goenv`` command is installed.
To use ``goof`` command, load functions there with ``source`` command or add following line in your .bashrc::

   export GOENVHOME=/path/to/environment_root
   source /path/to/goenvwrapper.sh

``goof`` command has follwing 5 subcommands.

``make`` creates environment with specified environment name under $GOENVHOME::

   % goof make foo
   (go:foo) % 

``make`` option accepts ``goenv`` command options, so you can create GAE skeleton project with following command for instance::

   % goof make foo -gae


``show`` shows all existing environments. Environment with '*' mark is current environment::

   (go:foo) % goof show
   bar
   foo *

``workon`` switches current enviroment to speficied environment::

   (go:foo) % goof workon bar
   (go:bar) %
   bar *
   foo

To delete unnecessary environment, run ``remove`` with the environment name::

   (go:bar) % goof remove foo
   bar *


4.2 Go install support
----------------------

From goenv version 0.4.0, goof supports installing Go itself. In order to ``goinstall`` subcommand, set ``GOENVGOROOT`` environment variable::

   % export GOENVGOROOT=/opt/goenv

``goinstall`` subcommand works with tag of the Go repository, such as "go1.0.3", "release.r60" and "weekly.2012-01-27".
For first call of ``goof goinstall``, it fetches all source from Go repository into $GOENVGOROOT::

   % goof goinstall go1.0.3
   [goof] Go source code initial checkout.
   [goof] 'go1.0.3' is installed in /opt/goenv/go1.0.3

All installed Go binaries are shown with ``goinstall`` subcommand without arguments::

   % goof goinstall
     /opt/goenv/go (repository)
     /opt/goenv/go1.0.2
   * /opt/goenv/go1.0.3
     /opt/goenv/release

You can also uninstall Go binaries with ``-u`` option before version number::

   % goof goinstall -u go1.0.2
   [goof] uninstalled version 'go1.0.2'

When you need to confirm all tags, give "tags" as an argument::

   % goof goinstall tags
   tip                            15392:7bc28d72d49c
   release                        13677:2d8bc3c94ecb
   go1.0.3                        13677:2d8bc3c94ecb
   go1.0.2                        13230:5e806355a9e1
   go1.0.1                        12994:2ccfd4b451d3
   go1                            12872:920e9d1ffd1f
   ...

``goof go`` replaces current GOROOT with specified Go version::

   % goof go go1.0.2
   % goof goinstall
     /opt/goenv/go (repository)
     /opt/goenv/go1.0.2
   * /opt/goenv/go1.0.3
     /opt/goenv/release


4.3 Advanced option
-------------------

``goof make`` is supporting ``gocode`` and ``godef`` installation which are code auto-completion tool for and code search assistance for go respectively.
Either of them will be installed under ``GOPATH/bin``::

   % goof make test -gocode -godef
   % ls $GOPATH/bin/
   gocode godef


5. Contribution
===============

Any contribution to this project are welcome. 
If you are to send patch to this repository, please request to develop branch, because this project is developed under `A Successful Git branching model <http://nvie.com/posts/a-successful-git-branching-model/>`_.
All patches to other branches than develop will be ignored unless those are application for hotfix branches.


6. License
==========

``goenv`` is an open source project with BSD-style license. See ``LICENSE`` file for details.
